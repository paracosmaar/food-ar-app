//
//  FoodViewController.swift
//  FoodApp
//
//  Created by mac on 3/14/21.
//

import UIKit

class FoodViewController: UIViewController,UITabBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tabBar: UITabBar!
    var fooddata:[FoodData]?
    var selectedindex = 0
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        fooddata = DataDecoder.getTrigonometryData(filename: "fooddata")
        searchBar.delegate = self
        collectionView.reloadData()
        tabBar.delegate = self
       
        // Do any additional setup after loading the view.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        switch item.tag {
        case 1:
            
            let sb = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MoreViewController")
            sb.modalPresentationStyle = .fullScreen
            present(sb, animated: false, completion: nil)
            
        default:
            print("item ",item.tag)
        }
        
      
    }
    

   

}


extension FoodViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
  
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return fooddata?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FoodCollectionViewCell
        cell.setUpCells(foodata: self.fooddata![indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height/1.5)
            
        case .phone:
               return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height)
        default:
              return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height)
        }
     
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

        self.selectedindex = indexPath.row
        
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        let arController = segue.destination as! ViewController
        
        let cell = sender as! FoodCollectionViewCell
        let index = self.collectionView!.indexPath(for: cell)
        
        arController.indexselected = index!.row
       
    }
    
    
}

extension FoodViewController:UITextFieldDelegate,UISearchBarDelegate{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           self.view.endEditing(true)
           return false
       }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
