//
//  AppData.swift
//  FoodApp
//
//  Created by mac on 3/15/21.
//

import Foundation

struct FoodData:Decodable {
    
    var title:String
    var subtitle:String
    var image:String
    var rate:String
    
    
}
