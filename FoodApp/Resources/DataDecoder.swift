//
//  DataDecoder.swift
//  TrigonometryFormulas
//
//  Created by VRLab on 7/10/19.
//  Copyright © 2019 KTM Studio. All rights reserved.
//

import Foundation

class DataDecoder {
    
   class func getTrigonometryData(filename fileName: String) -> [FoodData]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([FoodData].self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}




