//
//  FoodCollectionViewCell.swift
//  FoodApp
//
//  Created by mac on 3/14/21.
//

import UIKit

class FoodCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var rate: UILabel!
    
    
    override func awakeFromNib() {
        
        self.container.layer.cornerRadius = 10
    }
    
    
    func setUpCells(foodata:FoodData){
        
        title.text = foodata.title
        subTitle.text = foodata.subtitle
        imageView.image = UIImage(named: foodata.image)
        rate.text = foodata.rate
    }
}
