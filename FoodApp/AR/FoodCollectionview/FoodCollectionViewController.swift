//
//  FoodCollectionViewController.swift
//  FoodApp
//
//  Created by mac on 3/15/21.
//

import UIKit

class FoodCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}



extension FoodCollectionViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FoodSelectionCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FoodSelectionCollectionViewCell
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
              return CGSize(width: collectionView.frame.width/2.4, height: collectionView.frame.height)
            
        case .phone:
               return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height)
        default:
              return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height)
        }
     
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
    }
    
    
}
