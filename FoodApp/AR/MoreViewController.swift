//
//  CartViewController.swift
//  Store App
//
//  Created by Bikash on 5/24/18.
//  Copyright © 2018 prayogshaala. All rights reserved.
//

import UIKit
import SafariServices

class MoreViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    @IBAction func websiteTapped(_ sender: Any) {
        let sf = SFSafariViewController(url: URL(string: "https://www.google.com")!)
         sf.modalPresentationStyle = .fullScreen
        self.present(sf, animated: true, completion: nil)
    }
    
    
}
