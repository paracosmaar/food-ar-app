/*
 See LICENSE folder for this sample’s licensing information.
 
 Abstract:
 Main view controller for the AR experience.
 */

import ARKit
import SceneKit
import UIKit

class ViewController: UIViewController,SelectedObject,CAAnimationDelegate {
    
    @IBOutlet weak var containerView: UIView!
    
    var bottomanchorconstorigin:NSLayoutConstraint?
    var bottomanchorconstbottom:NSLayoutConstraint?
    @IBOutlet weak var sliderLeadingConstant: NSLayoutConstraint!
    
    @IBOutlet var sceneView: VirtualObjectARView!
    

    @IBOutlet weak var addObjectButton: UIButton!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
 
    
    @IBOutlet weak var upperControlsView: UIView!
    @IBOutlet weak var slider: UISlider!
    
    // MARK: - UI Elements
    var isexpand = false
    var expandanim = CAAnimation()
    var collapseanim = CAAnimation()
    
    var rotateValue = 0.0
    
    let coachingOverlay = ARCoachingOverlayView()
    
    var focusSquare = FocusSquare()
    
    /// The view controller that displays the status and "restart experience" UI.
    lazy var statusViewController: StatusViewController = {
        return children.lazy.compactMap({ $0 as? StatusViewController }).first!
    }()
    
    lazy var foodCollectionViewController: FoodCollectionViewController = {
        return children.lazy.compactMap({ $0 as? FoodCollectionViewController }).first!
    }()
    
    /// The view controller that displays the virtual object selection menu.
    var objectsViewController: VirtualObjectSelectionViewController?
    
    // MARK: - ARKit Configuration Properties
    
    /// A type which manages gesture manipulation of virtual content in the scene.
    lazy var virtualObjectInteraction = VirtualObjectInteraction(sceneView: sceneView, viewController: self)
    
    /// Coordinates the loading and unloading of reference nodes for virtual objects.
    let virtualObjectLoader = VirtualObjectLoader()
    
    /// Marks if the AR experience is available for restart.
    var isRestartAvailable = true
    
    /// A serial queue used to coordinate adding or removing nodes from the scene.
    let updateQueue = DispatchQueue(label: "com.example.apple-samplecode.arkitexample.serialSceneKitQueue")
    
    /// Convenience accessor for the session owned by ARSCNView.
    var session: ARSession {
        return sceneView.session
    }
    var indexselected = 0
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        sceneView.session.delegate = self
        sliderLeadingConstant.constant = (sliderLeadingConstant.constant - slider.bounds.width/2)
        slider.transform = CGAffineTransform(rotationAngle: -(.pi/2))
        // Set up coaching overlay.
        setupCoachingOverlay()
        virtualObjectInteraction.objdelegate = self
        sceneView.scene.rootNode.addChildNode(focusSquare)
        statusViewController.restartExperienceHandler = { [unowned self] in
            self.slider.value = 0

            self.restartExperience()
        }
        addObjectButton.imageView?.contentMode = .scaleAspectFit
        statusViewController.clearObjects = {
            [unowned self] in
            self.slider.value = 0
        
            self.virtualObjectLoader.removeAllVirtualObjects()
        }
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(loadObjectList))
        tapGesture.delegate = self
        sceneView.addGestureRecognizer(tapGesture)

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        resetTracking()
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        session.pause()
    }
    
    
    func selectedObject() {

//        slider.value = abs(Float((virtualObjectInteraction.selectedObject?.objectheight)!))
    }
    
    
    
    @IBAction func BackButton(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func sliderButton(_ sender: UISlider) {
        
        ObjectHeight(heightvalue: sender.value)
        
    }
    
    

    
    
    func animationFromSceneNamed(path: String) -> CAAnimation? {
        let scene  = SCNScene(named: path)
        var animation:CAAnimation?
        scene?.rootNode.enumerateChildNodes({ child, stop in
            if let animKey = child.animationKeys.first {
                animation = child.animation(forKey: animKey)
                stop.pointee = true
            }
        })
        return animation
    }
    
    // MARK: - Session management
    
    /// Creates a new AR configuration to run on the `session`.
    func resetTracking() {
        virtualObjectInteraction.selectedObject = nil
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        if #available(iOS 12.0, *) {
            configuration.environmentTexturing = .automatic
        }
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        
        statusViewController.scheduleMessage("FIND A SURFACE TO PLACE AN OBJECT", inSeconds: 7.5, messageType: .planeEstimation)
    }
    
    // MARK: - Focus Square
    
    func updateFocusSquare(isObjectVisible: Bool) {
        if isObjectVisible || coachingOverlay.isActive {
            focusSquare.hide()
        } else {
            focusSquare.unhide()
            statusViewController.scheduleMessage("TRY MOVING LEFT OR RIGHT", inSeconds: 5.0, messageType: .focusSquare)
        }
        
        // Perform ray casting only when ARKit tracking is in a good state.
        if let camera = session.currentFrame?.camera, case .normal = camera.trackingState,
            let query = sceneView.getRaycastQuery(),
            let result = sceneView.castRay(for: query).first {
            
            updateQueue.async {
                self.sceneView.scene.rootNode.addChildNode(self.focusSquare)
                self.focusSquare.state = .detecting(raycastResult: result, camera: camera)
            }
            if !coachingOverlay.isActive {
          
                addObjectButton.isHidden = false
            }
            statusViewController.cancelScheduledMessage(for: .focusSquare)
        } else {
            updateQueue.async {
                self.focusSquare.state = .initializing
                self.sceneView.pointOfView?.addChildNode(self.focusSquare)
            }
            
            
            addObjectButton.isHidden = true
            objectsViewController?.dismiss(animated: false, completion: nil)
        }
    }
    
    // MARK: - Error handling
    
    func displayErrorMessage(title: String, message: String) {
        // Blur the background.
        blurView.isHidden = false
        
        // Present an alert informing about the error that has occurred.
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let restartAction = UIAlertAction(title: "Restart Session", style: .default) { _ in
            alertController.dismiss(animated: true, completion: nil)
            self.blurView.isHidden = true
            self.resetTracking()
        }
        alertController.addAction(restartAction)
        present(alertController, animated: true, completion: nil)
    }
    
}


extension SCNGeometry {
    class func line(from vector1: SCNVector3, to vector2: SCNVector3) -> SCNGeometry {
        let indices: [Int32] = [0, 1]
        let source = SCNGeometrySource(vertices: [vector1, vector2])
        let element = SCNGeometryElement(indices: indices, primitiveType: .line)
        return SCNGeometry(sources: [source], elements: [element])
    }
}
